FROM registry.gitlab.com/emulation-as-a-service/emulators/emulators-base

LABEL "EAAS_EMULATOR_TYPE"="fs-uae"
LABEL "EAAS_EMULATOR_VERSION"="git+eaas-01032019"

run apt-add-repository ppa:fengestad/stable

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
fs-uae \
fs-uae-launcher \
fs-uae-arcade

ADD metadata /metadata
